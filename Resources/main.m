//
//  main.m
//  Temiz
//
//  Created by Denis Dovgan on 11/1/16.
//  Copyright © 2016 Tundra Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
