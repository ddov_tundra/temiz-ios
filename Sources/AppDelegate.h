//
//  AppDelegate.h
//  Temiz
//
//  Created by Denis Dovgan on 11/1/16.
//  Copyright © 2016 Tundra Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

