//
//  TMViewController.m
//  Temiz
//
//  Created by Denis Dovgan on 11/1/16.
//  Copyright © 2016 Tundra Mobile. All rights reserved.
//

@import PureLayout;

#import "TMViewController.h"

@interface TMViewController ()

@end

@implementation TMViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	self.view.backgroundColor = [UIColor whiteColor];
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


@end
